/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.oxgame2;

import java.util.Scanner;

/**
 *
 * @author nasan
 */
public class main {

    static String[] table = {"-", "-", "-", "-", "-", "-", "-", "-", "-",};
    static String turn = "o";
    static String winner = null;
    static int count = 0;
    static Scanner kb = new Scanner(System.in);

    public static void main(String[] args) {
        showWelcome();
        if (checkStart(kb.next()) == false) {
            return;
        }
        while (winner == null) {
            table = selectPosition(table, turn);
            turn=switchTurn(turn);
            winner = checkResult(table, winner, count);
            count++;
            if (winner != null) {
                showCheckRestartText();
                if (checkRestart(kb.next()) == true) {
                    setNewGame();
                }
            }
        }
    }

    public static void showWelcome() {
        System.out.println("==================");
        System.out.println("Welcome to OX game");
        System.out.println("==================");
        System.out.println("Press F to start.");
        System.out.println("==================");
    }

    public static boolean checkStart(String input) {
        if (input.equals("F") || input.equals("f")) {
            return true;
        } else {
            return false;
        }
    }

    private static void showCheckRestartText() {
        System.out.println("     Press R to play again.");
        System.out.println("     ======================");
        System.out.println("     Press any key to exit.");
    }

    public static boolean checkRestart(String input) {
        if (input.equals("R") || input.equals("r")) {
            return true;
        } else {
            return false;
        }
    }

    private static void setNewGame() {
        for (int i = 0; i < 9; i++) {
            table[i] = "-";
        }
        turn = "o";
        winner = null;
        count = 0;
    }

    public static void showTable() {
        System.out.println("==========");
        for (int i = 0; i < 3; i++) {
            System.out.print(table[i] + " ");
        }
        System.out.println("");
        for (int i = 3; i < 6; i++) {
            System.out.print(table[i] + " ");
        }
        System.out.println("");
        for (int i = 6; i < 9; i++) {
            System.out.print(table[i] + " ");
        }
        System.out.println("");
        System.out.println("==========");
    }

    public static String checkResult(String[] table, String winner, int count) {
        for (int i = 0; i < 8; i++) {
            String three = "";

            switch (i) {
                case 0 ->
                    three = table[0] + table[1] + table[2];
                case 1 ->
                    three = table[3] + table[4] + table[5];
                case 2 ->
                    three = table[6] + table[7] + table[8];
                case 3 ->
                    three = table[0] + table[3] + table[6];
                case 4 ->
                    three = table[1] + table[4] + table[7];
                case 5 ->
                    three = table[2] + table[5] + table[8];
                case 6 ->
                    three = table[0] + table[4] + table[8];
                case 7 ->
                    three = table[2] + table[4] + table[6];
            }

            if (three.equals("xxx")) {
                winner = "x";
                System.out.println("========|| Winner : X ||========");
                return winner;
            } else if (three.equals("ooo")) {
                winner = "o";
                System.out.println("========|| Winner : O ||========");
                return winner;
            } else if (winner == null) {
                if (count == 8) {
                    winner = "draw";
                    System.out.println("========|| Draw ||========");
                    return winner;
                }
            }
        }
        return winner;
    }

    public static String[] selectPosition(String[] table, String turn) {

        boolean valid ;
        int row;
        int col;

        do {
            System.out.println("Turn " + turn);
            System.out.println("Select your position");
            System.out.println("row : ");
            row = kb.nextInt();
            System.out.println("col : ");
            col = kb.nextInt();

            if (row == 1 && col == 1) {
                if (!table[0].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[0] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 1 && col == 2) {
                if (!table[1].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[1] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 1 && col == 3) {
                if (!table[2].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[2] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 2 && col == 1) {
                if (!table[3].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[3] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 2 && col == 2) {
                if (!table[4].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[4] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 2 && col == 3) {
                if (!table[5].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[5] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 3 && col == 1) {
                if (!table[6].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[6] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 3 && col == 2) {
                if (!table[7].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[7] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else if (row == 3 && col == 3) {
                if (!table[8].equals("-")) {
                    System.out.println("This position has been taken,Please try again.");
                    valid = false;
                } else {
                    table[8] = turn;
                    valid = true;
                    showTable();
                    return table;
                }
            } else {
                System.out.println("===============|| Out of range,Please try again. ||===============");
                valid = false;
            }
        } while (valid == false);

//        showTable();

        return table;
    }

    public static String switchTurn(String turn) {
        if (turn.equals("o")) {
            turn = "x";
        } else {
            turn = "o";
        }
        return turn;
    }

}
