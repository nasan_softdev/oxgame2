package com.nasan.oxgame2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author nasan
 */
public class mainTest {

    public mainTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void checkWinVerticalO1Test() {
        String[] table = {"o", "-", "-", "o", "-", "-", "o", "-", "-"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinVerticalO2Test() {
        String[] table = {"-", "o", "-", "-", "o", "-", "-", "o", "-"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinVerticalO3Test() {
        String[] table = {"-", "-", "o", "-", "-", "o", "-", "-", "o"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    // Horizontal unit test
    @Test
    public void checkWinHorizontalO1Test() {
        String[] table = {"o", "o", "o", "-", "-", "-", "-", "-", "-"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinHorizontalO2Test() {
        String[] table = {"-", "-", "-", "o", "o", "o", "-", "-", "-"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinHorizontalO3Test() {
        String[] table = {"-", "-", "-", "-", "-", "-", "o", "o", "o"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    // Diagonal unit test
    @Test
    public void checkWinDiagonalO1Test() {
        String[] table = {"o", "-", "-", "-", "o", "-", "-", "-", "o"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinDiagonalO2Test() {
        String[] table = {"-", "-", "o", "-", "o", "-", "o", "-", "-"};
        String winner = null;
        assertEquals("o", main.checkResult(table, winner, 0));
    }

    // Draw unit test
    @Test
    public void checkDrawTest() {
        String[] table = {"o", "x", "o", "x", "o", "o", "x", "o", "x"};
        String winner = null;
        assertEquals("draw", main.checkResult(table, winner, 8));
    }

    // Check x vertical win
    @Test
    public void checkWinVerticalX1Test() {
        String[] table = {"x", "-", "-", "x", "-", "-", "x", "-", "-"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinVerticalX2Test() {
        String[] table = {"-", "x", "-", "-", "x", "-", "-", "x", "-"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinVerticalX3Test() {
        String[] table = {"-", "-", "x", "-", "-", "x", "-", "-", "x"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    // Horizontal unit test
    @Test
    public void checkWinHorizontalX1Test() {
        String[] table = {"x", "x", "x", "-", "-", "-", "-", "-", "-"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinHorizontalX2Test() {
        String[] table = {"-", "-", "-", "x", "x", "x", "-", "-", "-"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinHorizontalX3Test() {
        String[] table = {"-", "-", "-", "-", "-", "-", "x", "x", "x"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    // Diagonal unit test
    @Test
    public void checkWinDiagonalX1Test() {
        String[] table = {"x", "-", "-", "-", "x", "-", "-", "-", "x"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    @Test
    public void checkWinDiagonalX2Test() {
        String[] table = {"-", "-", "x", "-", "x", "-", "x", "-", "-"};
        String winner = null;
        assertEquals("x", main.checkResult(table, winner, 0));
    }

    // check start game unit test
    @Test
    public void checkCheckStartEnterGame() {
        assertEquals(true, main.checkStart("F"));
        assertEquals(true, main.checkStart("f"));
    }

    @Test
    public void checkCheckStartExitGame() {
        assertEquals(false, main.checkStart("g"));
    }

    // check restart game unit test
    @Test
    public void checkRestartReGame() {
        assertEquals(true, main.checkRestart("r"));
        assertEquals(true, main.checkRestart("R"));
    }

    @Test
    public void checkRestartExitGame() {
        assertEquals(false, main.checkRestart("g"));
    }

     //check switch turn
    @Test
    public void checkSwitchTurn() {
        assertEquals("x", main.switchTurn("o"));
        assertEquals("o", main.switchTurn("x"));
    }
}
